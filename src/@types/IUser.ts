import type { ITodo } from './ITodos';

export interface IUser {
	name: string;
	email: string;
	todos: ITodo[];
}
