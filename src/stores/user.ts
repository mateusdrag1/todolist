import { get } from 'svelte/store';
import type { IUser } from 'src/@types/IUser';
import { browser } from '$app/env';
import { users } from './users';

import { writable, type Writable } from 'svelte/store';

let storedUser = null;
if (browser) {
	const storedUserJson = localStorage.getItem('TODO@user');

	storedUser = storedUserJson ? JSON.parse(storedUserJson) : [];
}

export const user: Writable<IUser | null> = writable(storedUser);

user.subscribe((value) => {
    if (!value || !browser)
        return;

    localStorage.setItem('TODO@user', JSON.stringify(value));
    console.log("value", value);

    let anySet = false;
    let newUsers = get(users).map((user) => {
        if (user.email === value.email) {
            anySet = true;
            return value;
        }
        return user;
    });

    if (!anySet)
        newUsers.push(value);
    users.set(newUsers);
})
