import type { IUser } from 'src/@types/IUser';
import { browser } from '$app/env';

import { writable, type Writable } from 'svelte/store';

let storedUsers = [];
if (browser) {
	const storedUsersJson = localStorage.getItem('TODO@users');

	storedUsers = storedUsersJson ? JSON.parse(storedUsersJson) : [];
}

export const users: Writable<IUser[]> = writable(storedUsers);

if (browser) {
    users.subscribe((value) => {
        localStorage.setItem('TODO@users', JSON.stringify(value));
    });
}
